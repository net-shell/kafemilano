var keystone = require('keystone');
var User = keystone.list('User');

module.exports = function (done) {
	new User.model({
		name: {
			first: 'Boyan',
			last: 'Balkanski'
		},
		email: 'ash@microweber.com',
		password: 'ash42$@',
		isAdmin: true
	})
	.save(done);
};
