# Cafe Diem #

Business process management software specialized for coffee shops.

### How do I get set up? ###

* Clone the repository.
* Install dependencies with `npm install`.
* Create `.env` file in the root directory and set your database connection string, for example `MONGO_URI=mongodb://localhost:27017/cafediem`

### Deployment instructions ###

The `.env` file should look like this:

```
PORT=3003
DOMAIN_ID=59ad2828d7c72713c0159ab6
URL_BASE=http://127.0.0.1:3003
```

### Contribution guidelines ###

* Writing tests
* Code review
* Suggestions

### Who do I talk to? ###

* ash+cafediem@microweber.com
* balkanski+cafediem@outlook.com