const keystone = require('keystone');
const Product = keystone.list('Product');
const ProductReview = keystone.list('ProductReview');

exports = module.exports = function (req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'menu';
	locals.filters = {
		product: req.params.product,
	};

	// Load the current product
	view.on('init', function (next) {
		Product.model.findOne({ state: 'published', key: locals.filters.product })
			.populate('categories domain')
			.exec(function (err, result) {
				locals.product = result;
				locals.pageStyle = (result.categories[0].backgroundImage && result.categories[0].backgroundImage.filename ? 'background-image: url(/media/'+ result.categories[0].backgroundImage.filename +')' : '');
				locals.pageClass = 'bg-wrapper';
				next(err);
			});
	});

	// Load other products
	view.on('init', function (next) {
		Product.model.find()
			.where('state', 'published')
			.where('categories').in([locals.product.categories[0]._id])
			.sort('-price')
			.populate('categories domain')
			.limit(6)
			.exec(function (err, result) {
				locals.products = result;
				next(err);
			});
	});

	// Create a review
	view.on('post', { action: 'review.create' }, function (next) {
		var newReview = new ProductReview.model({ product: locals.product.id });
		newReview.getUpdateHandler(req).process(req.body, { fields: 'rating, text', flashErrors: true, logErrors: true }, function (err) {
			if(err) validationErrors = err.errors;
			else {
				req.flash('success', 'Благодарим Bи за отзива!');
				return res.redirect('/menu/product/'+ locals.product.key);
			}
			next();
		});
	});

	view.render('product');
}
