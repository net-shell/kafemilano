const keystone = require('keystone');
const Banner = keystone.list('Banner');
const Category = keystone.list('Category');
const Product = keystone.list('Product');

exports = module.exports = function (req, res) {
	var view = new keystone.View(req, res);
	res.locals.section = 'home';

	// Load banners
	view.query('banners', Banner.model.find().where('isActive', true).sort('sortOrder').populate('category'));

	// Load featured products
	view.query('featuredProducts', Product.model.find()
		.where('state', 'published')
		.where('isFeatured', true)
		.sort('sortOrder')
		.populate('categories domain'));

	// Load new products
	view.query('newProducts', Product.model.find()
		.where('state', 'published')
		.sort('-updatedAt')
		.populate('categories domain')
		.limit(4));

	// Load categories
	view.query('categories', Category.model.find().sort('sortOrder'));

	view.render('index');
}
