const keystone = require('keystone');
const Table = keystone.list('Table');

exports = module.exports = function (req, res) {
    Table.model.findOne({ key: req.params.qr }).exec(function(err, table) {
        if(!table) {
            req.flash('error', "Грешен QR код. Моля, опитайте отново.");
        } else {
            res.cookie('table', table._id, { maxAge: 900000, httpOnly: true });
        }
        res.redirect('/');
    });
}
