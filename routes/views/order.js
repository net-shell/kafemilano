const keystone = require('keystone');
const Page = keystone.list('Page');

exports = module.exports = function (req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.filters = {
		page: req.params.page,
	};
	locals.section = locals.filters.page;

	// Load the current page
	view.query('page', Page.model.findOne({ key: locals.filters.page}));

	view.render('page');
}
