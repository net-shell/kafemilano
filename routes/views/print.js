const keystone = require('keystone');
const Table = keystone.list('Table');
const qr = require('qr-image');

exports = module.exports = function (req, res) {
    var view = new keystone.View(req, res);

    view.on('init', function(next) {
        Table.model.find().lean().sort('sortOrder').exec(function(err, results) {
            for(var r in results) {
                var url = process.env.URL_BASE +'/qr/'+ results[r].key;
                var image = qr.imageSync(url, { type: 'png', ec_level: 'H' });
                results[r].qr = new Buffer(image).toString('base64');
                results[r].url = url;
            }
            res.locals.tables = results;
            next();
        });
    });


    view.render('print');
}
