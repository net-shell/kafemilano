const keystone = require('keystone');
const Product = keystone.list('Product');
const Category = keystone.list('Category');

exports = module.exports = function (req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	locals.section = 'menu';
	locals.filters = {
		category: req.params.category,
		search: req.query.search || ''
	};
	locals.products = [];
	locals.categories = [];

	// Load categories
	view.query('categories', Category.model.find({ parent: null }).sort('sortOrder'));

	// Load the current category filter
	view.on('init', function (next) {
		if(!req.params.category) {
			return next();
		}
		Category.model.findOne({ key: locals.filters.category }).populate('parent').exec(function (err, result) {
			if(!result) return;
			locals.category = result;
			if(result.backgroundImage && result.backgroundImage.filename) {
				locals.pageStyle = 'background-image: url(/media/'+ result.backgroundImage.filename +')';
				locals.pageClass = 'bg-wrapper';
			}
			next(err);
		});
	});

	// Load subcategories
	view.on('init', function (next) {
		if(!locals.category) return next();
		var conditions = [{ parent: locals.category._id }];
		if(locals.category.parent)
			conditions.push({ parent: locals.category.parent._id });
		Category.model.find({ $or: conditions }).sort('sortOrder').exec(function (err, results) {
			if(err || !results || !results.length) return next(err);
			locals.subcategories = results;
			next(err);
		});
	});

	// Load the products
	view.on('init', function (next) {
		var findWhat = {};
		if(locals.filters.search.length) {
			// Filter by text
			var filter = { $regex: locals.filters.search, $options: 'i' };
			findWhat = { $or: [
				{ 'key': filter },
				{ 'name': filter },
				{ 'details.description': filter },
				{ 'categories.name': filter },
				{ 'categories.key': filter },
			]};
		}
		var query = Product.model.find(findWhat)
			.where('state', 'published')
			.sort('sortOrder')
			.populate('categories domain')
		if(locals.category) {
			// Filter by category
			var categories = [locals.category];
			if(!locals.category.parent && locals.subcategories) {
				for(var s in locals.subcategories) {
					categories.push(locals.subcategories[s]._id);
				}
			}
			query.where('categories').in(categories);
		} else {
			query.limit(42);
		}
		query.exec(function (err, results) {
			locals.products = results;
			next(err);
		});
	});

	view.render('menu');
}
