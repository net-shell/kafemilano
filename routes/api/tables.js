const keystone = require('keystone');
const Table = keystone.list('Table');

exports = module.exports = {
	index: function (req, res) {
		Table.model.find(function (err, results) {
			if (err) { throw err; }
			res.json(results);
		});
	},

	create: function (req, res) {
		var table = new Table.model({
			name: req.body.name,
			seats: req.body.seats
		});
		table.save(function(err, table) {
			if(!table) return res.json({ error: 'No table found' });
			res.json({ created: table });
		});
	},

	update: function (req, res) {
		Table.model.findById(req.body._id, function(err, table) {
			if(!table) return res.json({ error: 'No table found' });
			table.getUpdateHandler(req).process(req.body, {
				fields: 'name, seats, drawData',
				logErrors: true,
			}, function (err) {
				if (err)
					return res.json({ error: err });
				res.json({ updated: table });
			});
		});
	}
};