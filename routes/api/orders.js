const keystone = require('keystone');
const Order = keystone.list('Order');

exports = module.exports = {
	index: function (req, res) {
		Order.model.find(function (err, results) {
			if (err) { throw err; }
			res.json(results);
		});
	},

	create: function (req, res) {
		var order = new Order.model({
			domain: res.locals.domain._id,
			table: res.locals.table._id,
			products: [req.body.product]
		});
		var ok = req.body.product;
		order.save(function (err) {
			if (err)return res.json({ error: err });
			res.cookie('activeOrder', JSON.stringify(order), { maxAge: 900000, httpOnly: true });
			console.log('cookie created successfully');
			res.json({ created: order });
		});
	},

	update: function (req, res) {
		Order.model.findById(req.body._id, function(err, order) {
			if(!order) return res.json({ error: 'Order not found' });
			order.getUpdateHandler(req).process(req.body, {
				fields: 'employee, products, acceptedAt, paidAt',
				logErrors: true,
			}, function (err) {
				if (err)
					return res.json({ error: err });
				res.json({ updated: order });
			});
		});
	}
};