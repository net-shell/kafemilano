const _ = require('lodash');
const keystone = require('keystone');
const Domain = keystone.list('Domain');
const Page = keystone.list('Page');
const Table = keystone.list('Table');

exports.theme = function (req, res, next) {
	if (req.query.theme) {
		req.session.theme = req.query.theme;
	}
	res.locals.themes = ['Bootstrap', 'Cerulean', 'Cosmo', 'Cyborg', 'Darkly', 'Flatly', 'Journal', 'Lumen', 'Paper', 'Readable', 'Sandstone', 'Simplex', 'Slate', 'Spacelab', 'Superhero', 'United', 'Yeti'];
	res.locals.currentTheme = req.session.theme || 'Darkly';
	next();
};

exports.flashMessages = function (req, res, next) {
	var flashMessages = {
		info: req.flash('info'),
		success: req.flash('success'),
		warning: req.flash('warning'),
		error: req.flash('error')
	};
	res.locals.messages = _.any(flashMessages, function (msgs) { return msgs.length }) ? flashMessages : false;
	next();
};

exports.menuPages = function (req, res, next) {
	Page.model.find({ showInMenu: true }).lean().sort('sortOrder').exec(function (err, pages) {
		for(var p in pages) {
			res.locals.navLinks.push({
				label: pages[p].name,
				key: pages[p].key,
				href: '/page/' + pages[p].key
			});
		}
		next();
	});
};

exports.activeDomain = function (req, res, next) {
	Domain.model.findById(process.env.DOMAIN_ID).exec(function(err, domain) {
		if(domain) {
			res.locals.domain = domain;
			if(!res.locals.pageStyle || !res.locals.pageStyle.length)
				res.locals.pageStyle = 'background-color: '+ domain.background.color +'; background-image: url(/media/'+ (domain.background.image ? domain.background.image.filename : '') +');';
		}
		next();
	});
};

exports.activeTable = function (req, res, next) {
	if(req.cookies.table === undefined) {
		return next();
	}
	Table.model.findById(req.cookies.table).exec(function(err, table) {
		res.locals.table = table;
		next();
	});
};

exports.activeOrder = function (req, res, next) {
	var cookie = req.cookies.activeOrder;
	if (cookie !== undefined) {
		Order.model.findById(cookie).exec(function(err, order) {
			res.locals.order = order;
			next();
		});
		console.log('cookie exists', cookie);
	} 
	next();
}