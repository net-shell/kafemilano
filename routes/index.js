const keystone = require('keystone');
const middleware = require('./middleware');
const importRoutes = keystone.importer(__dirname);

keystone.pre('routes', function (req, res, next) {
	res.locals.navLinks = [
		{ label: 'Меню', key: 'menu', href: '/menu' },
		{ label: 'Отзив', key: 'feedback', href: '/feedback' },
	];
	res.locals.user = req.user;
	res.locals.domain = { name: 'MOCK' };
	next();
});

keystone.pre('render', middleware.theme);
keystone.pre('render', middleware.flashMessages);
keystone.pre('render', middleware.menuPages);
keystone.pre('routes', middleware.activeDomain);
keystone.pre('routes', middleware.activeTable);
keystone.pre('routes', middleware.activeOrder);

keystone.set('404', function (req, res, next) {
    middleware.theme(req, res, next);
	res.status(404).render('errors/404');
});

// Load Routes
var routes = {
	api: importRoutes('./api'),
	views: importRoutes('./views'),
};

exports = module.exports = function (app) {
	// Map "/media" to URL
	var mediaUrl = process.env.MEDIA_URL;
	if(mediaUrl && mediaUrl.length) {
		app.get('/media/:file', function(req, res) {
			res.redirect(mediaUrl +'/'+ req.params.file);
		});
	}

	// Views
	app.get('/', routes.views.index);
	app.get('/menu/:category?', routes.views.menu);
	app.all('/menu/product/:product', routes.views.product);
	app.get('/page/:page', routes.views.page);
	app.get('/qr/:qr', routes.views.qr);
	app.all('/feedback', routes.views.feedback);
	app.get('/orders', routes.views.orders);
	app.get('/print', routes.views.print);

	// API
	app.get('/api/tables', routes.api.tables.index);
	app.post('/api/tables/new', routes.api.tables.create);
	app.post('/api/tables', routes.api.tables.update);
	app.get('/api/orders', routes.api.orders.index);
	app.post('/api/orders/new', routes.api.orders.create);
	app.post('/api/orders', routes.api.orders.update);
}
