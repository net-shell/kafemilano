function TableCanvas(canvasId, vm)
{
    var me = this;

    this.canvasId = canvasId;
    this.canvas = new fabric.Canvas(canvasId, { selection: false });
    this.data = [];
    this.groups = [];
    this.vm = vm;

    this.canvas.on('selection:cleared', function() {
        me.vm.selection = null;
    });

    this.getTableGroup = function(table, drawData) {
        table.color = '#99ff00';
        if(table.drawData && table.drawData.length) {
            var tableDrawData = JSON.parse(table.drawData);
            if(tableDrawData) drawData = tableDrawData;
        }
        drawData.angle = -10;
        drawData.hasControls = false;
        drawData.hasRotatingPoint = false;
        drawData.padding = 4;
        drawData.borderColor = '#000000';
        var circle = new fabric.Circle({
            originX: 'center', originY: 'center',
            fill: table.color,
            radius: 16
        });
        var text = new fabric.Text(table.name, {
            originX: 'center', originY: 'center',
            fontSize: 16,
            fontWeight: 'bold',
            textAlign: 'center'
        });
        var group = new fabric.Group([circle, text], drawData);
        group.table = table;
        group.on('selected', function() {
            me.vm.selection = this.table;
        });
        group.on('modified', function() {
            if(!me.vm.selection) return;
            var newDrawData = { left: this.left, top: this.top };
            me.vm.selection.drawData = JSON.stringify(newDrawData);
        });
        return group;
    }

    this.draw = function() {
        if(!me.canvas) return;
        me.canvas.clear();
        if(!me.data || !me.data.length) return;
        for(var d in me.data) {
            var r = parseInt(d/3);
            var drawData = { left: 36*r - (r*36), top: 6+36*r };
            var group = me.getTableGroup(me.data[d], drawData);
            me.groups.push(group);
            me.canvas.add(group);
        }
        return me;
    }
}