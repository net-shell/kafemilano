var keystone = require('keystone');
var Types = keystone.Field.Types;

var imageStorage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: keystone.expandPath('public/media'),
        publicPath: '/media',
    }
});

var Category = new keystone.List('Category', {
	autokey: { from: 'name', path: 'key', unique: true },
	sortable: true,
	track: { createdAt: true, updatedAt: true },
	label: "Категория", plural: "Категории"
});

Category.add({
	name: { type: String, label: "Име", required: true },
	domain: { type: Types.Relationship, label: "Обект", required: true, initial: true, ref: 'Domain' },
	parent: { type: Types.Relationship, label: "Подкатегория на", initial: true, ref: 'Category' },
	defaultProductImage: { type: Types.File, label: "Снимка на продукт по подразбиране", storage: imageStorage },
	backgroundImage: { type: Types.File, label: "Снимка за фон", storage: imageStorage },
	description: { type: Types.Html, label: "Описание", wysiwyg: true }
});

Category.relationship({ path: 'products', ref: 'Product', refPath: 'categories' });

Category.defaultColumns = 'name, parent, domain|20%';
Category.register();
