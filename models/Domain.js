var keystone = require('keystone');
var Types = keystone.Field.Types;

var imageStorage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: keystone.expandPath('public/media'),
        publicPath: '/media',
    }
});

var Domain = new keystone.List('Domain', {
	autokey: { from: 'name', path: 'key', unique: true },
    track: { createdAt: true, updatedAt: true },
	label: "Обект", plural: "Обекти"
});

Domain.add({
	name: { type: Types.Text, label: "Име", required: true },
    slogan: { type: String, label: "Слоган" },
	logo: { type: Types.File, label: "Лого", storage: imageStorage },
	blueprints: { type: Types.File, label: "План", storage: imageStorage },
	defaultProductImage: { type: Types.File, label: "Снимка на продукт по подразбиране", storage: imageStorage },
	background: {
        image: { type: Types.File, label: "Снимка на фона", storage: imageStorage },
	    color: { type: Types.Color, label: "Цвят на фона" },
    },
    url: { type: String }
});

Domain.relationship({ path: 'employees', ref: 'Employee', refPath: 'domain' });
Domain.relationship({ path: 'enquiries', ref: 'Enquiry', refPath: 'domain' });
Domain.relationship({ path: 'orders', ref: 'Order', refPath: 'domain' });
Domain.relationship({ path: 'pages', ref: 'Page', refPath: 'domain' });
Domain.relationship({ path: 'products', ref: 'Product', refPath: 'domain' });
Domain.relationship({ path: 'tables', ref: 'Table', refPath: 'domain' });

Domain.defaultColumns = 'name, logo';
Domain.register();
