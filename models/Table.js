var keystone = require('keystone');
var Types = keystone.Field.Types;

var Table = new keystone.List('Table', {
    autokey: { from: 'name', path: 'key', unique: true },
	track: true,
	label: "Маса", plural: "Маси"
});

Table.add({
	name: { type: String, label: "Име", initial: true, required: true, index: true },
	domain: { type: Types.Relationship, label: "Обект", required: true, initial: true, ref: 'Domain' },
	seats: { type: Types.Number, label: "Места", initial: true, index: true },
    drawData: { type: String, hidden: true, default: null },
});

Table.relationship({ path: 'orders', ref: 'Order', refPath: 'table' });

Table.defaultColumns = 'name, seats, domain|20%';
Table.register();
