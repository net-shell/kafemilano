var keystone = require('keystone');
var Types = keystone.Field.Types;

var imageStorage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: keystone.expandPath('public/media'),
        publicPath: '/media',
    }
});

var Product = new keystone.List('Product', {
	autokey: { from: 'name', path: 'key', unique: true },
	sortable: true,
	track: true,
	label: "Продукт", plural: "Продукти"
});

Product.add({
	name: { type: String, label: "Име", required: true },
	isFeatured: { type: Types.Boolean, label: "Препоръчан", index: true },
	price: { type: Types.Money, label: "Цена", format: '0.00 лв', initial: true, index: true },
	categories: { type: Types.Relationship, label: "Категории", ref: 'Category', many: true, initial: true },
	details: {
		volume: { type: Types.Number, label: "Обем", initial: true, note: "в милилитри" },
		weight: { type: Types.Number, label: "Тегло", initial: true, note: "в грама" },
		description: { type: Types.Html, label: "Описание", wysiwyg: true }
	},
	image: { type: Types.File, label: "Снимка", initial: true, storage: imageStorage },
	domain: { type: Types.Relationship, label: "Обект", required: true, initial: true, ref: 'Domain' },
	state: {
		type: Types.Select,
		label: "Статус",
		options: [{ value: 'draft', label: "Чернова"}, { value: 'published', label: "Активен"}, { value: 'archived', label: "Скрит" }],
		default: 'published',
		index: true
	},
	additionFor: { type: Types.Relationship, label: "Добавка за продукт", ref: 'Product', many: true },
});

Product.schema.virtual('imageFile').get(function() {
	if(this.image && this.image.filename)
		return this.image.filename;
	var defaultImage = this.domain.defaultProductImage ? this.domain.defaultProductImage.filename : '';
	if(this.categories.length && this.categories[0].defaultProductImage && this.categories[0].defaultProductImage.filename)
		defaultImage = this.categories[0].defaultProductImage.filename;
	return defaultImage;
});

Product.relationship({ path: 'reviews', ref: 'ProductReview', refPath: 'product' });
Product.relationship({ path: 'options', ref: 'ProductOption', refPath: 'product' });

Product.defaultColumns = 'name, isFeatured|5%, categories, price|10%, details.volume|10%, details.weight|10%, updatedAt';
Product.register();
