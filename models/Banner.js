var keystone = require('keystone');
var Types = keystone.Field.Types;

var imageStorage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: keystone.expandPath('public/media'),
        publicPath: '/media',
    }
});

var Banner = new keystone.List('Banner', {
	autokey: { from: 'name', path: 'key', unique: true },
	sortable: true,
	defaultColumns: 'name, isActive, category, domain|20%',
	track: { createdAt: true, updatedAt: true },
	label: "Банери", singular: "Банер", plural: "Банери",
});

Banner.add({
	name: { type: String, label: "Заглавие", required: true, initial: true },
	text: { type: String, label: "Текст", initial: true },
	isActive: { type: Types.Boolean, label: "Активен", required: true, initial: true },
	image: { type: Types.File, label: "Изображение", storage: imageStorage, initial: true },
	category: { type: Types.Relationship, label: "Връзка към категория", note: "Ако е празно ще води към менюто.", initial: true, ref: 'Category' },
	domain: { type: Types.Relationship, label: "Обект", required: true, initial: true, ref: 'Domain' },
});

Banner.schema.virtual('imageFile').get(function() {
	return this.image.filename
		|| (this.category && this.category.defaultProductImage ? this.category.defaultProductImage.filename : null);
});

Banner.register();
