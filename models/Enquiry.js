var keystone = require('keystone');
var Types = keystone.Field.Types;

var Enquiry = new keystone.List('Enquiry', {
	nocreate: true,
	track: { createdAt: true },
	label: "Отзив", plural: "Отзиви"
});

Enquiry.add({
	name: { type: Types.Name, required: true, label: "Име" },
	phone: { type: String, label: "Телефон" },
	enquiryType: { type: Types.Select, options: [
		{ value: 'question', label: "Въпрос" },
		{ value: 'suggestion', label: "Предложение" },
		{ value: 'complain', label: "Оплакване" },
		{ value: 'other', label: "Нещо друго" },
	], required: true },
	domain: { type: Types.Relationship, label: "Обект", required: true, initial: true, ref: 'Domain' },
	message: { type: Types.Textarea, label: "Съобщение", required: true },
});

Enquiry.track = { createdAt: true };
Enquiry.defaultSort = '-createdAt';
Enquiry.defaultColumns = 'name, phone, enquiryType, createdAt';
Enquiry.register();
