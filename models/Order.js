var keystone = require('keystone');
var Types = keystone.Field.Types;

var Order = new keystone.List('Order', {
	autokey: { from: 'createdAt', path: 'key', unique: true },
	track: { createdAt: true },
	label: "Поръчка", plural: "Поръчки"
});

Order.add({
	domain: { type: Types.Relationship, label: "Обект", required: true, initial: true, ref: 'Domain' },
	table: { type: Types.Relationship, label: "Маса", required: true, initial: true, ref: 'Table' },
	employee: { type: Types.Relationship, label: "Служител", initial: true, ref: 'Employee' },
	products: { type: Types.Relationship, label: "Продукти", required: true, initial: true, many: true, ref: 'Product' },
	acceptedAt: { type: Types.Datetime, label: "Приета", index: true },
	paidAt: { type: Types.Datetime, label: "Платена", index: true },
});

Order.defaultColumns = 'createdAt|20%, table, acceptedAt|20%, paidOn|20%, domain';
Order.register();
