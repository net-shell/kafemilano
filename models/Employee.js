var keystone = require('keystone');
var Types = keystone.Field.Types;

var imageStorage = new keystone.Storage({
    adapter: keystone.Storage.Adapters.FS,
    fs: {
        path: keystone.expandPath('public/media'),
        publicPath: '/media',
    }
});

var Employee = new keystone.List('Employee', {
    autokey: { from: 'name', path: 'key', unique: true },
    track: true,
    label: "Служител", plural: "Служители"
});

Employee.add({
    name: { type: Types.Name, label: "Име", required: true, initial: true, index: true },
	domain: { type: Types.Relationship, label: "Обект", required: true, initial: true, ref: 'Domain' },
    image: { type: Types.File, label: "Снимка", storage: imageStorage },
	tables: { type: Types.Relationship, label: "Маси", initial: true, many: true, ref: 'Table' },
	password: { type: Types.Password, label: "Парола", initial: true, required: true },	
});

Employee.relationship({ path: 'orders', ref: 'Order', refPath: 'employee' });

Employee.defaultColumns = 'name, image, tables, domain|20%';
Employee.register();
