var keystone = require('keystone');
var Types = keystone.Field.Types;

var Page = new keystone.List('Page', {
	autokey: { from: 'name', path: 'key', unique: true },
	sortable: true,
	track: true,
	label: "Страница", plural: "Страници"
});

Page.add({
	name: { type: String, label: "Заглавие", initial: true, required: true },
	domain: { type: Types.Relationship, label: "Обект", required: true, initial: true, ref: 'Domain' },
	showInMenu: { type: Types.Boolean, label: "Показване в менюто", index: true },
	content: { type: Types.Html, label: "Съдържание", wysiwyg: true, height: 600 },
});

Page.defaultColumns = 'name, domain, showInMenu';
Page.register();
