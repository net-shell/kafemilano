var keystone = require('keystone');
var Types = keystone.Field.Types;

var ProductReview = new keystone.List('ProductReview', {
	track: { createdAt: true },
	label: "Отзив за продукт", plural: "Отзиви за продукти"
});

ProductReview.add({
	rating: { type: Types.Number, label: "Оценка", initial: true, required: true, index: true },
	text: { type: Types.Text, label: "Съобщение", initial: true },
	product: { type: Types.Relationship, label: "Продукт", required: true, initial: true, ref: 'Product', index: true },
});

ProductReview.defaultColumns = 'rating, text, product, createdAt';
ProductReview.register();
