var keystone = require('keystone');
var Types = keystone.Field.Types;

var ProductOption = new keystone.List('ProductOption', {
	track: { createdAt: true, updatedAt: true },
	label: "Опция за продукт", plural: "Опции за продукти"
});

ProductOption.add({
	name: { type: Types.Text, label: "Име", initial: true },
	description: { type: Types.Textarea, label: "Описание", initial: true, index: true },
    options: { type: Types.Text, label: "Опции", note: "Разделени със запетая", initial: true, required: true, index: true },
	product: { type: Types.Relationship, label: "Продукт", required: true, initial: true, ref: 'Product', index: true },
});

ProductOption.defaultColumns = 'name, description, product, options, createdAt';
ProductOption.register();
