var keystone = require('keystone');
var Types = keystone.Field.Types;

var User = new keystone.List('User', {
	nodelete: true,
	track: true,
	label: "Потребител", plural: "Потребители"
});

User.add({
	name: { type: Types.Name, label: "Име", required: true, index: true },
	email: { type: Types.Email, initial: true, required: true, index: true, unique: true },
	password: { type: Types.Password, label: "Парола", initial: true, required: false },
}, 'Permissions', {
	isProtected: { type: Boolean, noedit: true },
});

User.schema.virtual('canAccessKeystone').get(function () {
	return true;
});

User.schema.methods.wasActive = function () {
	this.lastActiveOn = new Date();
	return this;
}

User.defaultColumns = 'name, email, createdAt, updatedAt';
User.register();
