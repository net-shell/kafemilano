require('dotenv').config();
var keystone = require('keystone');

keystone.init({
	'name': 'Cafe Diem',
	'brand': 'Cafe Diem',

	'favicon': 'public/favicon.ico',
	'less': 'public',
	'static': 'public',

	'views': 'templates/views',
	'view engine': 'jade',

	'auto update': true,
	'mongo': process.env.MONGO_URI || 'mongodb://localhost/cafediem',
	'wysiwyg images': true,
	'compress': true,
	'session': true,
	'auth': true,
	'user model': 'User',
	'cookie secret': process.env.COOKIE_SECRET || 'DiemCafe',
});

keystone.import('models');

keystone.set('locals', {
	_: require('lodash'),
	env: keystone.get('env'),
	utils: keystone.utils,
	editable: keystone.content.editable,
});

keystone.set('routes', require('./routes'));

keystone.set('nav', {
	'меню': ['products', 'product-options', 'categories'],
	'поръчки': ['orders', 'tables'],
	'отзиви': ['enquiries', 'product-reviews'],
	'съдържание': ['pages', 'banners'],
	'обекти': ['domains', 'employees', 'users']
});

keystone.start({
	onHttpCreated: function () {
		keystone.io = require('socket.io')(keystone.httpServer);
		setupSocket(keystone.io);
	}
});

// Socket server
function setupSocket(io) {
	var numUsers = 0;

	io.on('connection', function (socket) {
		var addedUser = false;
		console.log('io connection made')
		// when the client emits 'new message', this listens and executes
		socket.on('new message', function (data) {
			// we tell the client to execute 'new message'
			socket.broadcast.emit('new message', {
				username: socket.username,
				message: data
			});
		});

		// when the client emits 'add user', this listens and executes
		socket.on('add user', function (username) {
			if (addedUser) return;

			// we store the username in the socket session for this client
			socket.username = username;
			++numUsers;
			addedUser = true;
			socket.emit('login', {
				numUsers: numUsers
			});
			// echo globally (all clients) that a person has connected
			socket.broadcast.emit('user joined', {
				username: socket.username,
				numUsers: numUsers
			});
		});

		// when the client emits 'typing', we broadcast it to others
		socket.on('typing', function () {
			socket.broadcast.emit('typing', {
				username: socket.username
			});
		});

		// when the client emits 'stop typing', we broadcast it to others
		socket.on('stop typing', function () {
			socket.broadcast.emit('stop typing', {
				username: socket.username
			});
		});

		// when the user disconnects.. perform this
		socket.on('disconnect', function () {
			if (addedUser) {
				--numUsers;

				// echo globally that this client has left
				socket.broadcast.emit('user left', {
					username: socket.username,
					numUsers: numUsers
				});
			}
		});
	});
}